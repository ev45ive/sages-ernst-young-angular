
# Installation
node -v 
v16.13.1

npm -v 
8.1.2

code -v
1.40.1

git --version
git version 2.31.1.windows.1

Google Chrome	98.0.4758.102 

## NPM
https://semver.npmjs.com/
https://semver.org/

## Angular Devtools
https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh

## CLI vs NX

https://angular.io/cli

https://nx.dev/

MSYS_NO_PATHCONV=1 docker run -v $(pwd):/home node bash 

npx create-nx-workspace --help
npx create-nx-workspace --preset=angular --name ey 


npx create-nx-workspace --preset=angular

  Workspace name (e.g., org name)     ey
  Application name                    training        
  Default stylesheet format           SASS(.scss)  [ http://sass-lang.com   ]
  Default linter                      ESLint [ Modern linting tool ]
  Use Nx Cloud? (It's free and doesn't require registration.) No

https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console

# GIT

git remote add matt https://bitbucket.org/ev45ive/sages-ernst-young-angular.git
git pull matt master

## Nx commands
nx serve

> nx run training:serve:development

# Generate UI lib
nx g @nrwl/angular:lib ui

# Add a component
nx g @nrwl/angular:component button --project ui

## Angular Material
npm install @angular/material && npx nx g @angular/material:ng-add

√ Choose a prebuilt theme name, or "custom" for a custom theme: · indigo-pink
√ Set up global Angular Material typography styles? (y/N) · true
√ Set up browser animations for Angular Material? (Y/n) · true

## Flex layout
npm i -s @angular/flex-layout @angular/cdk


## Storybook
ng generate @nrwl/angular:storybook-configuration --name=training

ng run training:storybook

https://storybook.js.org/tutorials/intro-to-storybook/angular/en/composite-component/

## Playlists module
<!-- nx g m playlists --module=app --route=playlists --routing -->

npx nx generate module --name=playlists --module=app --route=playlists --routing

npx nx generate component --name=playlists/containers/PlaylistsView --module=playlists --style=scss --displayBlock --type=Container

npx nx generate component --name=playlists/components/PlaylistDetails --module=playlists --style=scss --displayBlock
npx nx generate component --name=playlists/components/PlaylistList --module=playlists --style=scss --displayBlock
npx nx generate component --name=playlists/components/PlaylistForm --module=playlists --style=scss --displayBlock


## Search module
npx nx generate module --name=search --module=app --route=search --routing

npx nx generate component --name=search/containers/AlbumSearchView --module=search --style=scss --displayBlock --type=Container

npx nx generate component --name=search/components/SearchForm --module=search --style=scss --displayBlock
npx nx generate component --name=search/components/SearchResultsGrid --module=search --style=scss --displayBlock
npx nx generate component --name=search/components/AlbumCard --module=search --style=scss --displayBlock

npx nx generate service --name=core/services/SearchAPI --no-flat

## Shared module
npx nx generate module --name=shared --module=playlists
npx nx generate component --name=shared/components/clock --style=none --export 

## Stories
npx nx generate @nrwl/angular:stories --name=training --no-generateCypressSpecs 

## Change detection
https://github.com/angular/angular/tree/master/packages/zone.js

## Component Tree Shaking
https://dev.to/this-is-angular/emulating-tree-shakable-components-using-single-component-angular-modules-13do
https://dev.to/angular/component-first-architecture-with-angular-and-standalone-components-3pjd
https://www.angulararchitects.io/en/aktuelles/angulars-future-without-ngmodules-part-2-what-does-that-mean-for-our-architecture/

## ng-template + Structural directives
https://material.angular.io/components/table/overview


## Value Accessor Directive

npx nx generate @schematics/angular:directive --name=shared/directives/contenteditable --project=training --module=shared --export 

## Layout + Projection
npx nx generate @schematics/angular:component --name=shared/layout/navigation --project=training --module=shared --style=scss --changeDetection=OnPush --displayBlock --export --skipTests 

npx nx generate @schematics/angular:component --name=shared/layout/center-layout --project=training --module=shared --style=scss --changeDetection=OnPush --displayBlock --export --skipTests 

## Core module
npx nx generate @schematics/angular:module --name=core --module=app 
npx nx generate @schematics/angular:service --name=core/services/playlistsAPI --no-flat

### S.O.L.I.D

https://en.wikipedia.org/wiki/SOLID

## RxJs
https://rxjs.dev/operator-decision-tree
https://rxjs.dev/api/operators/map
https://www.learnrxjs.io/learn-rxjs/operators/utility/delaywhen
https://rxviz.com/
https://rxmarbles.com/#scan

