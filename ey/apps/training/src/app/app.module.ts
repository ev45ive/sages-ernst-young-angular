import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UiModule } from '@ey/ui';

import { AppComponent } from './app.component';
import { NxWelcomeComponent } from './nx-welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterModule, Routes } from '@angular/router';
import { PlaylistsModule } from './playlists/playlists.module';
import { SearchModule } from './search/search.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { playlistsData } from './__mocks__/playlistsData';
import { environment } from '../environments/environment';
import { PlaylistsAPIService } from './core/services/playlists-api/playlists-api.service';
import { MockPlaylistsAPIService } from './__mocks__/MockPlaylistAppService';

const routes: Routes = [
  {
    path: 'playlists',
    loadChildren: () => import('./playlists/playlists.module')
      .then(m => m.PlaylistsModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module')
      .then(m => m.SearchModule)
  }
  // { path: '', component: HomeComponent },
  // { path: 'path', component: FeatureComponent },
  // { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [AppComponent, NxWelcomeComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    // PlaylistsModule,
    // SearchModule,
    UiModule,
    SharedModule,
    RouterModule.forRoot(routes),

    // environment.production ? [] : MocksModule
  ],
  providers: [
    environment.production ? [] : [{
      provide: 'INITIAL_PLAYLISTS',
      useValue: playlistsData
    },
    {
      provide: PlaylistsAPIService,
      useClass: MockPlaylistsAPIService
    }
  ]
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
