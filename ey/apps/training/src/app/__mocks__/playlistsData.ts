import { Playlist } from "../core/model/Playlist";

export const playlistsData:Playlist[] = [{
  id: '123',
  name: 'Playlist 123',
  public: true,
  description: "best playlist 123"
}, {
  id: '234',
  name: 'Playlist 234',
  public: false,
  description: "best playlist 234"
}, {
  id: '345',
  name: 'Playlist 345',
  public: true,
  description: "best playlist 345"
}];
