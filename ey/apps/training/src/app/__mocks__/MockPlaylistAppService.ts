import { Inject, Injectable, Optional } from '@angular/core';
import { Playlist } from '../core/model/Playlist';


// export class PlaylistsAPIService {}
// export class PlaylistsAPIServiceMock {}

@Injectable({
  providedIn: 'root',
})
export class MockPlaylistsAPIService {

  constructor(
    // @Optional() @Inject('INITIAL_PLAYLISTS') private playlists:Playlist[]
    @Inject('INITIAL_PLAYLISTS') private playlists: Playlist[]
  ) {
    // this.playlists = this.playlists || []
    console.log('Service is craeted');
  }

  getPlaylists(): Playlist[] {
    return this.playlists
  }

  getPlaylistById(id: Playlist['id']): Playlist | undefined {
    return this.playlists.find(p => p.id == id)
  }

  savePlaylist(draft: Playlist): Playlist {
    this.playlists = this.playlists.map(p =>
      p.id === draft.id ? draft : p
    )
    return draft
  }
}
