import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, EMPTY, exhaustMap, filter, map, mergeAll, mergeMap, share, Subject, Subscriber, Subscription, switchMap, takeUntil, tap } from 'rxjs';
import { Album } from '../../../core/model/Search';
import { SearchAPIService } from '../../../core/services/search-api/search-api.service';


@Component({
  selector: 'ey-album-search-view',
  templateUrl: './album-search-view.container.html',
  styleUrls: ['./album-search-view.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumSearchViewContainer implements OnInit {
  message = ''

  queryChanges = this.route.queryParamMap.pipe(
    map(p => p.get('q')),
    filter((q): q is string => q !== null),
    tap(() => {
      // some custom logic
    })
  )

  albumChanges = this.queryChanges.pipe(
    switchMap(query => {
      return this.service.searchAlbums(query).pipe(
        catchError(error => {
          this.message = error?.message
          return EMPTY // from([])
        }))
    }),
    share()
  )

  constructor(
    private service: SearchAPIService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  
  // query = '';
  ngOnInit(): void {
    // this.queryChanges.subscribe(query => this.query = query)
  }

  search(query: string) {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        q: query
      }
    })
  }
}
