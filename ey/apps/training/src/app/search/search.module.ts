import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SharedModule } from '../shared/shared.module';
import { AlbumSearchViewContainer } from './containers/album-search-view/album-search-view.container';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsGridComponent } from './components/search-results-grid/search-results-grid.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    component: SearchComponent,
    children: [
      {
        path: 'albums', component: AlbumSearchViewContainer
      },
      {
        path: '', pathMatch: 'full', redirectTo: 'playlists'
      }, {
        path: '**', redirectTo: 'playlists'
      },
    ]
  }
];

@NgModule({
  declarations: [
    SearchComponent,
    AlbumSearchViewContainer,
    SearchFormComponent,
    SearchResultsGridComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class SearchModule { }
