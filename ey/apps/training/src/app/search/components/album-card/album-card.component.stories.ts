import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { AlbumCardComponent } from './album-card.component';

export default {
  title: 'AlbumCardComponent',
  component: AlbumCardComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    })
  ],
} as Meta<AlbumCardComponent>;

const Template: Story<AlbumCardComponent> = (args: AlbumCardComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
}