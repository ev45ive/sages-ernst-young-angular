import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Album } from '../../../core/model/Search';

@Component({
  selector: 'ey-search-results-grid',
  templateUrl: './search-results-grid.component.html',
  styleUrls: ['./search-results-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsGridComponent implements OnInit {

  @Input() results: Album[] | null = [];

  constructor() { }

  ngOnInit(): void {
  }

}
