import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SearchResultsGridComponent } from './search-results-grid.component';

export default {
  title: 'SearchResultsGridComponent',
  component: SearchResultsGridComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    })
  ],
} as Meta<SearchResultsGridComponent>;

const Template: Story<SearchResultsGridComponent> = (args: SearchResultsGridComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
}