import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs';

@Component({
  selector: 'ey-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFormComponent implements OnInit {

  searchForm = new FormGroup({
    'query': new FormControl('batman', {
      validators: [
        Validators.required,
        Validators.minLength(3),
      ]
    }),
    advancedSearch: new FormGroup({
      type: new FormControl('album'),
    })
  })
  queryField = this.searchForm.get('query')

  @Input() set query(value: string | null) {
    (this.searchForm.get('query') as FormControl)?.setValue(value, {
      emitEvent: false // dont emit valueChanges
    })
  }

  @Output() search = new EventEmitter<string>();

  constructor() {
  }

  submit(value: string) {
    // this.searchForm.get('advancedSearch')?.disable()

    console.log(value);
    this.search.emit(value)
  }

  ngOnInit(): void {
    const field = this.searchForm.get('query');

    // Multicast (hot) observable:
    field?.valueChanges.pipe(
      // delay 400ms after last change,
      debounceTime(400),
      // send longer or equal than 3 letters
      filter(query => query.length >= 3),
      // avoid duplicates 
      distinctUntilChanged(),

    )
      // .subscribe(val => console.log(val));

      // .subscribe({
      //   next: (value) => this.search.next(value)
      //   error: (value) => this.search.error(value)
      //   complete: () => this.search.complete()
      // });

      .subscribe(this.search); // chaining multicast observables!

  }

}
