import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SearchFormComponent } from './search-form.component';

export default {
  title: 'SearchFormComponent',
  component: SearchFormComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    })
  ],
} as Meta<SearchFormComponent>;

const Template: Story<SearchFormComponent> = (args: SearchFormComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
}