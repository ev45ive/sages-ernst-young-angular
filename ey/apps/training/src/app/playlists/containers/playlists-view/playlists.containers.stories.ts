import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SharedModule } from '../../../shared/shared.module';
import { PlaylistDetailsComponent } from '../../components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from '../../components/playlist-form/playlist-form.component';
import { PlaylistListComponent } from '../../components/playlist-list/playlist-list.component';
import { PlaylistsModule } from '../../playlists.module';
import { PlaylistsViewContainer } from './playlists-view.container';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export default {
  title: 'Playlists/Container',
  component: PlaylistsViewContainer,
  decorators: [
    moduleMetadata({
      declarations: [
        PlaylistListComponent,
        PlaylistDetailsComponent,
        PlaylistFormComponent
      ],
      imports: [SharedModule,
      BrowserAnimationsModule],
    })
  ],
} as Meta<PlaylistsViewContainer>;

const Template: Story<PlaylistsViewContainer> = (args: PlaylistsViewContainer) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
}