import { Component, Inject, OnInit } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';
import { playlistsData } from '../../../__mocks__/playlistsData';
import { PlaylistsAPIService } from '../../../core/services/playlists-api/playlists-api.service';

@Component({
  selector: 'ey-playlists-view',
  templateUrl: './playlists-view.container.html',
  styleUrls: ['./playlists-view.container.scss']
})
export class PlaylistsViewContainer implements OnInit {

  mode:
    | 'details'
    | 'edit' = 'details'

  playlists: Playlist[] = []
  selectedPlaylist?: Playlist

  constructor(
    // @Inject(PlaylistsAPIService)
    // private service: PlaylistsAPIService
    protected readonly service: PlaylistsAPIService
  ) {
    this.playlists = this.service.getPlaylists()
  }

  ngOnInit(): void {
    console.log('Hello again');

  }
  selectPlaylist(id: Playlist['id']): void {
    this.selectedPlaylist = this.service.getPlaylistById(id)
  }

  savePlaylist(draft: Playlist) {
    this.service.savePlaylist(draft)
    this.playlists = this.service.getPlaylists()
    this.selectPlaylist(draft.id)

    this.mode = 'details'
  }

  showEdit() {
    this.mode = 'edit'
  }

  showDetails() {
    this.mode = 'details'
  }
}
