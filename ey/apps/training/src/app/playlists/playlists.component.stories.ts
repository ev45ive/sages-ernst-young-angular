import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SharedModule } from '../shared/shared.module';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistsViewContainer } from './containers/playlists-view/playlists-view.container';
import { PlaylistsComponent } from './playlists.component';
import { PlaylistsModule } from './playlists.module';

export default {
  title: 'Playlists/Component',
  component: PlaylistsComponent,
  decorators: [
    moduleMetadata({
      declarations:[
        PlaylistsViewContainer
      ],
      imports: [SharedModule],
    })
  ],
} as Meta<PlaylistsComponent>;

const Template: Story<PlaylistsComponent> = (args: PlaylistsComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
}