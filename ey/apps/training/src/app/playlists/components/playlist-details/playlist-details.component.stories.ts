import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SharedModule } from '../../../shared/shared.module';
import { playlistsData } from '../../../__mocks__/playlistsData';
import { PlaylistDetailsComponent } from './playlist-details.component';

export default {
  title: 'Playlist/DetailsComponent',
  component: PlaylistDetailsComponent,
  decorators: [
    moduleMetadata({
      imports: [SharedModule],
    })
  ],
} as Meta<PlaylistDetailsComponent>;

const Template: Story<PlaylistDetailsComponent> = (args: PlaylistDetailsComponent) => ({
  props: args,
});


export const NoData = Template.bind({});
NoData.args = {
}

export const Primary = Template.bind({});
Primary.args = {
  playlist: playlistsData[1]
}