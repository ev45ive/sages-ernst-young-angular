import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

// type PlaylistId = Playlist['id']

@Component({
  selector: 'ey-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss'],
  // encapsulation: ViewEncapsulation.Emulated
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist?: Playlist

  // @Output() edit = new EventEmitter<string>();
  @Output() edit = new EventEmitter<Playlist['id']>();

  editClick() {
    this.edit.emit(this.playlist?.id)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
