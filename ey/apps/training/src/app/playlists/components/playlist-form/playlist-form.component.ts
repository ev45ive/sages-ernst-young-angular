import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { CheckboxControlValueAccessor, DefaultValueAccessor, FormControl, FormGroupDirective, NgForm, NgModel } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatInput } from '@angular/material/input';
import { Playlist } from '../../../core/model/Playlist';

MatInput
DefaultValueAccessor
CheckboxControlValueAccessor
NgForm
NgModel


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}



@Component({
  selector: 'ey-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
  // inputs: [], outputs: [], host: {}
})
export class PlaylistFormComponent implements OnInit {

  matcher = new MyErrorStateMatcher()

  @Input() playlist?: Playlist = {
    id: '',
    name: '',
    public: false,
    description: ""
  }
  draft?: Playlist;

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    this.draft = { ...this.playlist! }
  }


  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  savePlaylist(formRef: NgForm) {
    if (formRef.invalid) return;

    // formRef.control.enable()
    this.save.emit(this.draft)
  }

  @ViewChild('formRef', { read: NgForm })
  formRefFromTemplate?: NgForm

  @ViewChild('nameRef', { read: ElementRef })
  formElemRef?: ElementRef<unknown>

  ngAfterViewInit(): void {
    debugger
    if (this.formElemRef?.nativeElement instanceof HTMLInputElement)
      this.formElemRef.nativeElement.focus()
    // setTimeout(() => {
    // this.formRefFromTemplate?.control.disable()
    // })
  }


  isAdvancedOptions = false
  isDisabled = false;

  constructor() { }

  ngOnInit(): void {
  }

}


// @Component({
//   selector: 'ey-changed-playlist-form',
//   templateUrl: './playlist-form2.component.html',
//   // styleUrls: ['./playlist-form.component.scss'],
//   // changeDetection: ChangeDetectionStrategy.OnPush,
//   // inputs: [], outputs: [], host: {}
// })
// export class MyChangedPlaylistFormComponent extends PlaylistFormComponent {

//   override savePlaylist(formRef: NgForm) {
//     super.savePlaylist(formRef)
//   }
// }