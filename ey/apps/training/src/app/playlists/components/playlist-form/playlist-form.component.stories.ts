import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SharedModule } from '../../../shared/shared.module';
import { playlistsData } from '../../../__mocks__/playlistsData';
import { PlaylistFormComponent } from './playlist-form.component';

export default {
  title: 'Playlist/FormComponent',
  component: PlaylistFormComponent,
  decorators: [
    moduleMetadata({
      imports: [SharedModule, BrowserAnimationsModule],
    })
  ],
} as Meta<PlaylistFormComponent>;

const Template: Story<PlaylistFormComponent> = (args: PlaylistFormComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
  playlist: playlistsData[1]
}