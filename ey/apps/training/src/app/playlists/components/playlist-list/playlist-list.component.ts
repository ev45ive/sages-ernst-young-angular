import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'ey-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistListComponent implements OnInit {

  @Input() playlists: Playlist[] = []

  @Output() selected = new EventEmitter<Playlist['id']>();

  select(playlist: Playlist) {
    this.selected.emit(playlist.id)
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
