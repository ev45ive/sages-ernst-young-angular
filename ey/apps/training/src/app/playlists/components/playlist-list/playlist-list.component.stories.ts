import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SharedModule } from '../../../shared/shared.module';
import { playlistsData } from '../../../__mocks__/playlistsData';
import { PlaylistListComponent } from './playlist-list.component';

export default {
  title: 'Playlist/ListComponent',
  component: PlaylistListComponent,
  decorators: [
    moduleMetadata({
      imports: [SharedModule],
    })
  ],
} as Meta<PlaylistListComponent>;

const Template: Story<PlaylistListComponent> = (args: PlaylistListComponent) => ({
  props: args,
});


export const EmptyList = Template.bind({});
EmptyList.args = {
  playlists: []
}

export const Primary = Template.bind({});
Primary.args = {
  playlists: playlistsData
}
export const Reversed = Template.bind({});
Reversed.args = {
  playlists: [...playlistsData].reverse()
}