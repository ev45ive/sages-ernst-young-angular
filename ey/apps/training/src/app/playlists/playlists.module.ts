import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './playlists.component';
import { PlaylistsViewContainer } from './containers/playlists-view/playlists-view.container';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: '', component: PlaylistsComponent }
];

@NgModule({
  declarations: [
    PlaylistsComponent,
    PlaylistsViewContainer,
    PlaylistDetailsComponent,
    PlaylistListComponent,
    PlaylistFormComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  exports:[
    // PlaylistsViewContainer
  ]
})
export class PlaylistsModule { }
