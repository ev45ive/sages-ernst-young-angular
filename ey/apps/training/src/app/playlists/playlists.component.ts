import { Component, OnInit } from '@angular/core';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';

@Component({
  selector: 'ey-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.css'],
  providers: [
    // Local Service:
    // { provide: PlaylistAPI, useClass: PlaylistAPI }
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } as MatFormFieldDefaultOptions }
  ]
})
export class PlaylistsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
