import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[eyBadWords]',
  providers: [{ provide: NG_VALIDATORS, useExisting: BadWordsDirective, multi: true }]
})
export class BadWordsDirective implements Validator {

  @Input() eyBadWords = 'batman'

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null {
    const hasError = String(control.value).includes(this.eyBadWords)

    return hasError ? { badword: this.eyBadWords } : null
  }

}
