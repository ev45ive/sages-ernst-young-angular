import { Directive, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

// .clasA.classB
// <div class="clasA classB"></div>

// [attributeA][attrb]
// <div attributeA="" attrb=""></div>

@Directive({
  // selector: '[eyContenteditable]'
  selector: '[contenteditable][ngModel]',
  providers:[
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ContenteditableDirective,
      multi:true // make it array
    }
  ]
})
export class ContenteditableDirective implements ControlValueAccessor {

  constructor(private elem: ElementRef) { }

  writeValue(value: string): void {
    this.elem.nativeElement.innerHTML = value
  }

  registerOnChange(fn: any): void {
    this.elem.nativeElement.addEventListener('input', (event: InputEvent) => {
      fn(this.elem.nativeElement.innerHTML)
    })
  }

  registerOnTouched(fn: any): void {
    this.elem.nativeElement.addEventListener('blur', (event: InputEvent) => {
      fn()
    })
  }

}
