import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';

@Component({
  selector: 'ey-clock',
  templateUrl: './clock.component.html',
})
export class ClockComponent implements OnInit {

  time = ''

  updateTime() {
    this.time = (new Date()).toLocaleTimeString()
  }
 
  constructor(
    private zone: NgZone,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {

    this.zone.runOutsideAngular(() => {
      setInterval(() => {
        this.updateTime()
        this.cdr.detectChanges()
      }, 1000)
    })
  }

}
