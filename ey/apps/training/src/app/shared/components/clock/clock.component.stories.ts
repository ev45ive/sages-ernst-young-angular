import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { ClockComponent } from './clock.component';

export default {
  title: 'ClockComponent',
  component: ClockComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    })
  ],
} as Meta<ClockComponent>;

const Template: Story<ClockComponent> = (args: ClockComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
}