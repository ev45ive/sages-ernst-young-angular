import { Component, OnInit, ChangeDetectionStrategy, Input, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ey-my-form-field',
  templateUrl: './my-form-field.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR, multi: true, useExisting: MyFormFieldComponent
    }
  ]
})
export class MyFormFieldComponent implements OnInit, ControlValueAccessor {

  @Input() label = ''

  value = ''

  valueChange = new EventEmitter()

  changed(event:any){
    this.valueChange.emit(event)
  }

  constructor() { }

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.valueChange.subscribe(fn)
  }

  registerOnTouched(fn: any): void {  
    // fn()
  }

  ngOnInit(): void {
  }

}
