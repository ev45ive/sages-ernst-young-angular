import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFormFieldComponent } from './my-form-field.component';

describe('MyFormFieldComponent', () => {
  let component: MyFormFieldComponent;
  let fixture: ComponentFixture<MyFormFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyFormFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
