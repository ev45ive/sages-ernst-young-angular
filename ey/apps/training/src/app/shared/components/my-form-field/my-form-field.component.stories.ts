import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { MyFormFieldComponent } from './my-form-field.component';

export default {
  title: 'MyFormFieldComponent',
  component: MyFormFieldComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    })
  ],
} as Meta<MyFormFieldComponent>;

const Template: Story<MyFormFieldComponent> = (args: MyFormFieldComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
    label:  '',
}