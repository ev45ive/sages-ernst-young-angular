import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { CenterLayoutComponent } from './center-layout.component';

export default {
  title: 'CenterLayoutComponent',
  component: CenterLayoutComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    })
  ],
} as Meta<CenterLayoutComponent>;

const Template: Story<CenterLayoutComponent> = (args: CenterLayoutComponent) => ({
  props: args,
});


export const Primary = Template.bind({});
Primary.args = {
}