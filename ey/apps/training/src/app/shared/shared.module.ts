import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { ClockComponent } from './components/clock/clock.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContenteditableDirective } from './directives/contenteditable.directive';
import { MyFormFieldComponent } from './components/my-form-field/my-form-field.component';
import { BadWordsDirective } from './directives/bad-words.directive';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { CenterLayoutComponent } from './layout/center-layout/center-layout.component'

@NgModule({
  declarations: [
    ClockComponent,
    ContenteditableDirective,
    MyFormFieldComponent,
    BadWordsDirective,
    NavigationComponent,
    CenterLayoutComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    CommonModule,
    MatButtonModule,
    MatListModule,
    MatDividerModule,
    MatIconModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    FlexLayoutModule.withConfig({})
  ],
  exports: [FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    ClockComponent,
    MatListModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    FlexLayoutModule,
    ContenteditableDirective,
    MyFormFieldComponent,
    BadWordsDirective,
    NavigationComponent,
    CenterLayoutComponent
  ]
})
export class SharedModule { }
