import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Inject, Injectable } from '@angular/core';
import { environment } from 'apps/training/src/environments/environment';
import { catchError, EMPTY, filter, map, Observable, of, pluck, Subscription, tap, throwError } from 'rxjs';
import { API_URL } from '../../core.module';
import { Album, AlbumSearchResponse, AlbumSimplified, assertSearchResponse, isAlbumSearchResponse } from '../../model/Search';
import { AuthService } from '../auth/auth.service';

export const albumsData: AlbumSimplified[] = [
  {
    id: '123', name: 'Album 123', type: 'album', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }]
  },
  {
    id: '234', name: 'Album 234', type: 'album', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/200' }]
  },
  {
    id: '345', name: 'Album 345', type: 'album', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }]
  },
  {
    id: '456', name: 'Album 456', type: 'album', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }]
  },
]

@Injectable({
  providedIn: 'root'
})
export class SearchAPIService {

  url = environment.apiUrl + 'search'

  constructor(
    private http: HttpClient,
  ) { }

  searchAlbums(query: string): Observable<Album[]> {

    return this.http.get(this.url, {
      params: {
        type: 'album', query
      },
    })
      .pipe(
        map(res => {
          assertSearchResponse(res)
          return res.albums.items
        }),
      )
  }
}
