import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'apps/training/src/environments/environment';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private oauth: OAuthService
  ) {
    this.oauth.configure(environment.authConfig)

  }

  init() {
    from(this.oauth.tryLogin())
      .subscribe(() => {
        if (!this.getToken()) {
          this.login();
        }
      })
  }

  login() {
    this.oauth.initImplicitFlow();
  }

  getToken() {
    return this.oauth.getAccessToken()
  }

}
