import { Inject, Injectable, Optional } from '@angular/core';
import { Playlist } from '../../model/Playlist';


// export class PlaylistsAPIService {}
// export class PlaylistsAPIServiceMock {}

@Injectable({
  providedIn: 'root',
})
export class PlaylistsAPIService {

  playlists: Playlist[] = []

  constructor() {
    
  }
  getPlaylists(): Playlist[] {
    return this.playlists
  }

  getPlaylistById(id: Playlist['id']): Playlist|undefined {
    throw new Error('not implemented')
  }

  savePlaylist(draft: Playlist): Playlist {
    throw new Error('not implemented')
  }
}
