import { TestBed } from '@angular/core/testing';

import { PlaylistsAPIService } from './playlists-api.service';

describe('PlaylistsAPIService', () => {
  let service: PlaylistsAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlaylistsAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
