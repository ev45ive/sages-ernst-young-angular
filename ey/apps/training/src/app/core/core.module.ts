import { InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth/auth.service';
import { ApiInterceptor } from './api/api.interceptor';

export const API_URL = new InjectionToken<string>('API_URL');

// export const API_URL1 = new InjectionToken<string>('API_URL');
// API_URL !== API_URL1

// S.O.L.I.D //

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OAuthModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    {
      provide: API_URL, useValue: environment.apiUrl
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },

    // Override 3rd party providers
    // {
    //   provide: HttpHandler, useClass: MyAwesomeHttpHandler
    // },

    // {
    //   provide: HttpClient, 
    //   useClass: MyOwnMuchBetterHttpClient
    // },

    // No manually providing - import Module with Providers!
    // HttpClient,
    // HttpHandler, .... 

    // {
    //   provide: 'API_URL', useValue: environment.apiUrl
    // },
    {
      provide: 'INITIAL_PLAYLISTS',
      useValue: [],
      // useFactory(){},
      // useClass: Classname,
      // useExisting:'ExistingToken'
    },
    // // production
    // {
    //   provide: PlaylistsAPIService,
    //   useClass: PlaylistsAPIService
    // },

    // // testing
    // {
    //   provide: PlaylistsAPIService,
    //   useClass: PlaylistsAPIServiceMock
    // },
    // {
    //   provide: 'PlaylistAPI',
    //   useClass: PlaylistsAPIService
    //   // useFactory(){
    //   //   const s = new PlaylistsAPIService()
    //   //     s.someSpecialConfiguration()
    //   //     return s
    //   // }
    // }
  ]
})
export class CoreModule {

  constructor(private auth: AuthService) {
    this.auth.init()
  }
}
