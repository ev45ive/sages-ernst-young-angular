import { ErrorHandler, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService,
    private errorService: ErrorHandler
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const authReq = request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    })

    return next.handle(authReq).pipe(
      catchError(err => {

        // this.errorService.handleError(err)
        // return EMPTY

        if (!(err instanceof HttpErrorResponse)) {
          // assert its spotify error ...
          return throwError(() => new Error('Unexpected server error'))
        }

        if(err.error.status === 401){
          this.auth.login()
        }

        return throwError(() => new Error(err.error?.error?.message))
      })
    )
  }
}
