
import { NgModuleMetadata } from '@storybook/angular/dist/ts3.9/client/preview/types';

export const moduleMetadata = (metadata: Partial<NgModuleMetadata>) =>
  (context: () => any) => ({
    moduleMetadata: metadata,
    ...context()
  });